const box = document.querySelector(".count-container");
let counter = 0;
box.textContent = `${counter}`;

const start = document.querySelector(".start");
const reset = document.querySelector(".reset");
const stop = document.querySelector(".stop");

let interval = null;

start.addEventListener("click", () => {
  if (!interval) {
    interval = setInterval(() => {
      counter = counter + 1;
      box.textContent = `${counter}`;
    }, 1000);
  }
});

reset.addEventListener("click", () => {
  counter = 0;
  clearInterval(interval);
  interval = null;
  box.textContent = `${counter}`;
});
stop.addEventListener("click", () => {
  clearInterval(interval);
  interval = null;
});
